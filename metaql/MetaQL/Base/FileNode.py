
from MetaQL.Base.Globals import *

# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class FileNode(MQLNode):
    
    # ----------------------------------------------------------------------------------------------
    def __init__ (self, parent, name):
        """constructor for file nodes"""        
        MQLNode.__init__(self)
        self.setParent(parent)
        self.setName(name)
        self.setAlias(name)
        path = self.getPathName()
        self.setDate(time.ctime(os.path.getmtime(path)))
        self.setSize(os.path.getsize(path))

        if os.path.isdir(path):
            self.setTypeId (MQLNodeType_Directory)
            from MetaQL.Base.DirectoryNode import *
            self.__class__ = DirectoryNode
        else:
            self.setTypeId(MQLNodeType_File)

    # ----------------------------------------------------------------------------------------------
    def hasValidPath (self):
        """returns true, if the node exists and it's path isn't a directory"""
        path = self.getPathName()
        return os.path.exists(path) and not os.path.isdir(path)
    
    # ----------------------------------------------------------------------------------------------
    def getPathName (self, sep = os.sep):
        """returns the full path of the node"""
        parent = self.getParent()
        if parent:
            return os.path.normpath(os.path.join(parent.getPathName(), self.getName()))
        else:
            return self.getName()

    # ----------------------------------------------------------------------------------------------
    def getPath (self):
        """returns the expanded path.path of the node"""
        parent = self.getParent()
        if parent:
            p = parent.getPath() / self.getName()
        else:
            p = path.path(self.getName())
        from MetaQL.Base.Globals import debug    
        debug(p)
        p.expand()
        debug(p)
        return p
    
    # ----------------------------------------------------------------------------------------------
    def hasChildren(self):
        """returns false"""
        return False
    
    # ----------------------------------------------------------------------------------------------
    def getSuffix (self):
        """returns the suffix of the node"""
        list = self.getName().split(".")
        if len(list) > 1:
            suffix = list[-1]
            return suffix
        else:
            return None
    
    # ----------------------------------------------------------------------------------------------
    def isActionValidForNode (self, action, node):
        return False
    
