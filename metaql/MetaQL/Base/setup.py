# !/usr/bin/env python

from distutils.core import setup

short_description="Nodes"
long_description="Nodes: ..."

setup(
  name="Nodes",
  version="",
  licence="",
  description=short_description,
  author="",
  author_email="",
  maintainer="",
  maintainer_email="",
  url="",
  package_dir={"MetaQL.Base": '.'},
  packages=["MetaQL.Base",
            ],
  long_description = long_description,
  )
