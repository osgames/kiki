# Modeling
from Modeling.CustomObject import CustomObject
from Modeling.Validation import ValidationException
from mx.DateTime import DateTimeFrom


class Type(CustomObject):
  """
  Types are objects ...
  """
  
  __implements__ = CustomObject.__implements__
  
  def __init__(self):
    "Initializer"
    # Note: if you modify this method, it is a strong requirement that
    # every parameter gets a default value, since the framework needs to be
    # able to instanciate an object with no parameter at all.
    self._name = None
  
  def entityName(self):
      "Used by the framework to link this object to its entity"
      return "type" # do not change


  # Attribute: name

  def getName(self):
    "Return the Type / name attribute value"
    self.willRead()
    return self._name
  
  def setName(self, name):
    "Change the Type / name attribute value"
    self.willChange()
    self._name  = name
  
  def validateName(self, value):
    "Edit this to enforce custom business logic"
    if 0: # your custom bizlogic
      raise ValidationException
    return
    

