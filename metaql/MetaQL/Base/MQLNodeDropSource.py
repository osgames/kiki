from wxPython.wx import *
from MetaQL.Base.MQLNode import *
from MetaQL.Base.Globals import debug
 
# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class MQLNodeDropSource (wxDropSource):
    """
      DnD source for MetaQL nodes
    """

    # ----------------------------------------------------------------------------------------------
    def __init__(self, win, node):

        import os
        if os.name == 'nt':
            wxDropSource.__init__(self, win)
        else:
            wxDropSource.__init__(self, win, wxIcon("icon.xpm", wxBITMAP_TYPE_XPM), 
            wxIcon("icon.xpm", wxBITMAP_TYPE_XPM), wxIcon("icon.xpm", wxBITMAP_TYPE_XPM))
        self.oldPos = None
        self.node = node
        self.memDC = wxMemoryDC()
        self.memBitmap = wxEmptyBitmap (self.node.getIconBitmap().GetWidth(), \
                                        self.node.getIconBitmap().GetHeight())
        self.memDC.SelectObject(self.memBitmap)
        
        import pickle
        data = pickle.dumps (self.node, 'bin')
        self.nodeData = wxCustomDataObject (wxCustomDataFormat ("MQL_NODE"))
        self.nodeData.SetData (data)
        self.SetData (self.nodeData)
                                            
        # TBD: create a file data here for windows ...
##        if os.name == 'dos' or os.name == 'nt':
##            fileData = wxFileDataObject()
##            fileData.AddFile (node.getPathName())
##            

    # ----------------------------------------------------------------------------------------------
    def DoDragDrop (self, flags = wxDrag_DefaultMove):
        # start dnd action
        from MetaQL.Base.NodeManager import NodeManager
        nm = NodeManager()
        nm.draggedNode = self.node
        result = wxDropSource.DoDragDrop(self, flags)
        nm.draggedNode = None
        # clear screen
        width, height = self.node.getIconBitmap().GetWidth(), self.node.getIconBitmap().GetHeight()
        dc = wxScreenDC()
        dc.StartDrawingOnTop()
        dc.Blit(self.oldPos[0], self.oldPos[1], width, height, self.memDC, 0, 0)
        dc.EndDrawingOnTop()
        # return drop result
        return result
                        
    # ----------------------------------------------------------------------------------------------
    def GiveFeedback (self, effect):

        bitmap = self.node.getIconBitmap()
        width, height = bitmap.GetWidth(), bitmap.GetHeight()
        newPos = wxGetMousePosition()
        
        dc = wxScreenDC()
        dc.StartDrawingOnTop()

        tmpDC = wxMemoryDC()
        tmpBitmap = wxEmptyBitmap(width, height)
        tmpDC.SelectObject(tmpBitmap)
        
        if self.oldPos:
            # restore screen on old position
            newRegion = wxRegion(newPos[0], newPos[1], width, height)
            oldRegion = wxRegion (self.oldPos[0], self.oldPos[1], width, height)
            region = wxRegion (self.oldPos[0], self.oldPos[1], width, height)
            region.SubtractRegion (newRegion)
            dc.SetClippingRegionAsRegion(region)
            dc.Blit(self.oldPos[0], self.oldPos[1], width, height, self.memDC, 0, 0)
            dc.DestroyClippingRegion()
            
            if oldRegion.IntersectRegion(newRegion):
                # move the overlapping part in memDC to avoid flicker
                tmpDC.BeginDrawing()
                tmpDC.Blit(self.oldPos[0] - newPos[0], self.oldPos[1] - newPos[1], width, height, self.memDC, 0, 0)
                tmpDC.EndDrawing()
        
                self.memDC.BeginDrawing()
                self.memDC.Blit(0, 0, width, height, tmpDC, 0, 0)
                self.memDC.EndDrawing()
        
        # blit screen on new position to memory DC
        self.memDC.BeginDrawing()
        
        if self.oldPos:
            # mask the overlapping part in memDC (avoid flicker part 2)
            region = wxRegion(0, 0, width, height)
            region.Subtract(self.oldPos[0] - newPos[0], self.oldPos[1] - newPos[1], width, height)
            self.memDC.SetClippingRegionAsRegion(region)
            self.memDC.Blit(0, 0, width, height, dc, newPos[0], newPos[1])
            self.memDC.DestroyClippingRegion()
        else:    
            self.memDC.Blit(0, 0, width, height, dc, newPos[0], newPos[1])
        
        self.memDC.EndDrawing()
        
        # overlay icon and screen content in temporary memory DC
        tmpDC.BeginDrawing()
        tmpDC.Blit(0,0,width,height,self.memDC,0,0)
        tmpDC.DrawBitmap(self.node.getIconBitmap(), 0, 0, True)
        tmpDC.EndDrawing()
        
        # blit the icon to the screen
        dc.Blit(newPos[0], newPos[1], width, height, tmpDC, 0, 0)
        
        dc.EndDrawingOnTop()
        self.oldPos = newPos
        return False
    
