
from wxPython.wx import *
from Modeling.EditingContext import EditingContext

class DBManager:
    
    def __init__(self):
        
        self.ec = EditingContext()
        
    def insert(self, object):
        
        self.ec.insertObject(object)

    def delete(self, object):
        
        self.ec.deleteObject(object)
                
    def save(self):
        
        self.ec.saveChanges()