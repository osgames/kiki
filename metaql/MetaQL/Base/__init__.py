# Load the model

import os, warnings, StringIO, traceback
os.environ['NOTIFICATION_CENTER_MULTIPLE_CALLBACKS_PER_OBSERVER'] = 'True'

from Modeling import ModelSet
from model_Nodes import model_src
try:
  if ModelSet.defaultModelSet().modelNamed("Nodes") is None:
    ModelSet.defaultModelSet().addModelFromXML({'string': model_src})
    model=ModelSet.defaultModelSet().modelNamed('Nodes')
except:
  exc=StringIO.StringIO()
  traceback.print_exc(file=exc)
  warnings.warn("Couldn't load model 'model_Nodes.xml'\nReason:\n%s"%exc.getvalue())
  del exc

del os, warnings, StringIO, traceback, model_src

# Or, alternatively: use the xml file (ok for dev. mode, not for install w/
# distutils)
#
# from Modeling import ModelSet
# import os, warnings, StringIO, traceback
# try:
#   if ModelSet.defaultModelSet().modelNamed("Nodes") is None:
#     from os import getcwd, path
#     mydir = os.path.abspath(os.path.dirname(__file__))
#     xmlmodelPath=path.join(mydir,'model_Nodes.xml')
#     ModelSet.defaultModelSet().addModelFromXML({'file': xmlmodelPath})
#     model=ModelSet.defaultModelSet().modelNamed('Nodes')
# except:
#   exc=StringIO.StringIO()
#   traceback.print_exc(file=exc)
#   warnings.warn("Couldn't load model 'model_Nodes.xml'\nReason:\n%s"%exc.getvalue())
#   del exc
# 
# del os, warnings, StringIO, traceback
