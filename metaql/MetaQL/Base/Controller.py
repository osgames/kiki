
# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class Controller(object):

    __controller = None
    __mimeTypeManager = None
    
    # ----------------------------------------------------------------------------------------------
    def __init__(self):
        
        if not Controller.__controller:
            Controller.__controller = self
            Controller.__mimeTypeManager = wxTheMimeTypesManager
            from NotificationFramework import NotificationCenter
            NotificationCenter.addObserver (self, Controller.nodeActivated, "MQL_NODE_ACTIVATED")
        
    # ----------------------------------------------------------------------------------------------
    def nodeActivated (self, notification):
        from MetaQL.Base.Globals import debug
        node = notification.object()
        from MetaQL.Base.Globals import FileNode        
        if node.__class__ == FileNode:
            suffix = node.getSuffix()
            if suffix:
                mimeType = self.__mimeTypeManager.GetFileTypeFromExtension(suffix)
                openCommand = mimeType.GetOpenCommand(node.getPathName())
                debug ("openCommand %s" % openCommand)
                if openCommand:
                    wxExecute (openCommand)
                else:
                    debug ("no command for suffix!")
            else:
                debug ("no suffix!")
            



