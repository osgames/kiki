
import os, sys, time, stat, math, types, mx.DateTime, path

from wxPython.wx import *

from MetaQL.Base.MQLNode import *
from MetaQL.Base.FileNode import *
from MetaQL.Base.DirectoryNode import *
from MetaQL.Base.Controller import *

from NotificationFramework import *

# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
def debug(msg=""):
    """logDebug message msg"""
    from wxPython.wx import *
    import sys, os
    
    frame = sys._getframe(1)
    class_name = ''
    if frame.f_locals.has_key('self'):
        class_name = frame.f_locals['self'].__class__.__name__ + "."
    if msg:
        if type(msg) == types.StringType:
            msg = " :: " + msg
        else:
            msg = " :: " + str(msg)
    
    file = os.path.basename(frame.f_code.co_filename)
    
    wxLogDebug ("%s:%03d %s%s%s" % \
        (file, frame.f_code.co_firstlineno, class_name, frame.f_code.co_name, msg))
        
    sys.__stdout__.write ("%s:%03d %s%s%s\n" % \
        (file, frame.f_code.co_firstlineno, class_name, frame.f_code.co_name, msg))

# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------

MQLSashViews = [{'name':'OpenGL', 'class': 'OpenGLView', 'icon': wxART_WARNING },
                {'name':'Tree', 'class': 'TreeBrowser', 'icon': wxART_HELP_SIDE_PANEL },
                {'name':'File', 'class': 'FileList', 'icon': wxART_FOLDER },
                {'name':'Console', 'class': 'Console', 'icon': wxART_REPORT_VIEW },
                {'name':'Shelf', 'class': 'Shelf', 'icon': wxART_REPORT_VIEW } ]

