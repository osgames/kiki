from wxPython.wx import *
from OpenGL.GLUT import *
from OpenGL.GL import *

from MetaQL.Base.DirectoryNode import *
from MetaQL.Base.Node import *

from math import *

# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class Renderer:
    
    # ----------------------------------------------------------------------------------------------
    def __init__(self):
        pass

# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class FileTreeRenderer (Renderer):

    # ----------------------------------------------------------------------------------------------
    def __init__(self, pickHandler):
        
        Renderer.__init__(self)
        self.displayList = 0
        self.pickHandler = pickHandler
        
    # ----------------------------------------------------------------------------------------------
    def reset (self):
        
        if self.displayList:
            glDeleteLists (self.displayList, 1)
            self.displayList = 0
                        
    # ----------------------------------------------------------------------------------------------
    def renderNode (self, node, depth=0, pickable_id=-1):
        
        if depth == 0 and self.displayList:
            glCallList (self.displayList)
            return
        elif depth == 0:
            self.displayList = glGenLists(1)
            if not self.displayList:
                wxLogDebug("unable to generate display list!")
                return
            glNewList (self.displayList, GL_COMPILE_AND_EXECUTE)
		
        if node.__class__ == FileNode:         
            glColor3f (1.0, 0.0, 0.0)
        else:
            glColor3f (0.0, 0.0, 1.0)

        pickable_id = self.pickHandler.addPickable(node)
        glPushName (pickable_id)
        
        glutSolidCube (1.0)
        
        if depth > 1: return
        
        glPushMatrix()
        
        glTranslatef(0,3,0)        
        glScalef(0.5, 0.5, 0.5)
        
        children = node.getChildren()
                
        if children and len(children):
        
            side = ceil(sqrt(len(children)))
            index = 0
                        
            for childNode in children:
                
                glPushMatrix()
                
                x = int(index % side)
                z = int(index / side)
                                                
                index = index + 1
                
                glTranslated(x*2 - ceil(side/2),0,z*2 - ceil(side/2))
                
                self.renderNode (childNode, depth+1, pickable_id)
                
                if childNode.__class__ == FileNode:            
                    glColor3f (1.0, 0.0, 0.0)
                else:
                    glColor3f (0.0, 0.0, 1.0)
                
                glDisable(GL_LIGHTING)
                glBegin(GL_LINES)
                glVertex3f(0,0,0)
                glVertex3f(-(x*2 - ceil(side/2)),-6,-(z*2 - ceil(side/2)))
                glEnd()
                glEnable(GL_LIGHTING)
                
                glPopMatrix()
                
                glPopName()
	
        glPopMatrix()
        
        if depth == 0:
            glPopName()
            glEndList()
            
    
    
    