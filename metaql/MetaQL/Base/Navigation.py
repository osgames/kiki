
from OpenGLContext.events import examinemanager
from OpenGLContext import trackball
from MetaQL.Base.Globals import *
from OpenGLContext import quaternion, dragwatcher
from math import *

# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class Vector3:
    
    def __init__(self, v):
        self.x, self.y, self.z = v.x, v.y, v.z
    
    def dot (self, v):
        return self.x * v.x + self.y * v.y + self.z * v.z
    
    def parallel (self, v):
        d = self.dot(v)
        return Vector3(d * v.x, d * v.y, d * v.z)
    
    def perpendicular (self, v):
        d = self.dot(v)
        return (self.x - d * v.x, self.y - d * v.y, self.z - d * v.z)
    
    def length (self):
        return sqrt(self.dot(self))
    
    def norm (self):
        l = self.length()
        if l != 0.0: 
            l = 1.0/l
            return Vector3(self.x * l, self.y * l, self.z * l)
        return Vector3(self)

# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class RotateTrackball(trackball.Trackball):
    
    def update( self, newX, newY ):

        x, y = self.watcher.fractions ( newX, newY )
        yRotation, xRotation = x * self.dragAngle, -y * self.dragAngle
                
        xRot = apply (quaternion.fromXYZR, self.xAxis + (xRotation,))
        yRot = apply (quaternion.fromXYZR, (0,1,0) + (yRotation,))

        pos = ((xRot * yRot) * self.vector) + self.center
        rot = self.originalQuaternion * xRot * yRot
        
        return pos, rot

# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class ZoomTrackball(trackball.Trackball):
    
    def update( self, newX, newY ):

        x, y = self.watcher.fractions ( newX, newY )
        if abs(x) > abs(y):
            z = x * 2
        else:
            z = y * 2
        
        zAxis = (self.xAxis[1]*self.yAxis[2]-self.xAxis[2]*self.yAxis[1],
                 self.xAxis[2]*self.yAxis[0]-self.xAxis[0]*self.yAxis[2],
                 self.xAxis[0]*self.yAxis[1]-self.xAxis[1]*self.yAxis[0])
                        
        pos = self.vector + (z * zAxis[0], z * zAxis[1], z * zAxis[2], 0)
                
        return pos, self.originalQuaternion

# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class MoveTrackball(trackball.Trackball):
    
    def update( self, newX, newY ):

        x, y = self.watcher.fractions ( newX, newY )
        x *= -2
        y *= -2
		
        pos = self.vector + (y * self.yAxis[0], y * self.yAxis[1], y * self.yAxis[2], 0) \
                          + (x * self.xAxis[0], x * self.xAxis[1], x * self.xAxis[2], 0)
                
        return pos, self.originalQuaternion

# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class NavigationManager(examinemanager.ExamineManager):

    def OnBind (self):
        examinemanager.ExamineManager.OnBind(self)
        self.client.setNavigationMode(True)

    def OnUnBind (self):
        examinemanager.ExamineManager.OnUnBind(self)
        self.client.setNavigationMode(False)
	    
# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class RotateManager(NavigationManager):
    
    def OnBuildTrackball (self, platform, center, event, width, height):
        self.trackball = RotateTrackball (platform.position, platform.quaternion, center,
                                    event.getPickPoint()[0],event.getPickPoint()[1], width, height)

	        
# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class ZoomManager(NavigationManager):
    
	def OnBuildTrackball (self, platform, center, event, width, height):
		self.trackball = ZoomTrackball (platform.position, platform.quaternion, center,
			event.getPickPoint()[0],event.getPickPoint()[1], width, height)

# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class MoveManager(NavigationManager):
    
	def OnBuildTrackball (self, platform, center, event, width, height):
		self.trackball = MoveTrackball (platform.position, platform.quaternion, center,
			event.getPickPoint()[0],event.getPickPoint()[1], width, height)
