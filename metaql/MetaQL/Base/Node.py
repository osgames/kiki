# Modeling
from Modeling.CustomObject import CustomObject
from Modeling.Validation import ValidationException
from mx.DateTime import DateTimeFrom


class Node(CustomObject):
  """
  Nodes are objects ...
  """
  
  __implements__ = CustomObject.__implements__
  
  def __init__(self):
    "Initializer"
    # Note: if you modify this method, it is a strong requirement that
    # every parameter gets a default value, since the framework needs to be
    # able to instanciate an object with no parameter at all.
    self._alias = None
    self._date = None
    self._name = None
    self._size = None
    self._typeId = None
    self._children=[]
    self._parent=None
    self._type=None
  
  def entityName(self):
      "Used by the framework to link this object to its entity"
      return "node" # do not change


  # Attribute: alias

  def getAlias(self):
    "Return the Node / alias attribute value"
    self.willRead()
    return self._alias
  
  def setAlias(self, alias):
    "Change the Node / alias attribute value"
    self.willChange()
    self._alias  = alias
  
  def validateAlias(self, value):
    "Edit this to enforce custom business logic"
    if 0: # your custom bizlogic
      raise ValidationException
    return
  
  # Attribute: date

  def getDate(self):
    "Return the Node / date attribute value"
    self.willRead()
    return self._date
  
  def setDate(self, date):
    "Change the Node / date attribute value"
    self.willChange()
    self._date  = date
  
  def validateDate(self, value):
    "Edit this to enforce custom business logic"
    if 0: # your custom bizlogic
      raise ValidationException
    return
 
  # Attribute: name

  def getName(self):
    "Return the Node / name attribute value"
    self.willRead()
    return self._name
  
  def setName(self, name):
    "Change the Node / name attribute value"
    self.willChange()
    self._name  = name
  
  def validateName(self, value):
    "Edit this to enforce custom business logic"
    if 0: # your custom bizlogic
      raise ValidationException
    return
  
  # Attribute: size

  def getSize(self):
    "Return the Node / size attribute value"
    self.willRead()
    return self._size
  
  def setSize(self, size):
    "Change the Node / size attribute value"
    self.willChange()
    self._size  = size
  
  def validateSize(self, value):
    "Edit this to enforce custom business logic"
    if 0: # your custom bizlogic
      raise ValidationException
    return
  
  # Attribute: typeId

  def getTypeId(self):
    "Return the Node / typeId attribute value"
    self.willRead()
    return self._typeId
  
  def setTypeId(self, typeId):
    "Change the Node / typeId attribute value"
    self.willChange()
    self._typeId  = typeId
  
  def validateTypeId(self, value):
    "Edit this to enforce custom business logic"
    if 0: # your custom bizlogic
      raise ValidationException
    return
    

  # Relationship: children

  def getChildren(self):
    "Returns the children relationship (toMany)"
    self.willRead()
    return self._children
  
  def addToChildren(self, object):
    "Add the children relationship (toMany)"
    if object not in self._children:
      self.willChange()
      _children=list(self._children)
      _children.append(object)
      self._children=tuple(_children)
  
  def removeFromChildren(self, object):
    "Remove the children relationship (toMany)"
    self.willChange()
    _children=list(self._children)
    _children.remove(object)
    self._children=tuple(_children)
  

  


  # Relationship: parent

  def getParent(self):
    "Return the parent relationship (toOne)"
    self.willRead()
    return self._parent
  
  def setParent(self, object):
    "Set the parent relationship (toOne)"
    self.willChange()
    self._parent=object
  
  

  # Relationship: type

  def getType(self):
    "Return the type relationship (toOne)"
    self.willRead()
    return self._type
  
  def setType(self, object):
    "Set the type relationship (toOne)"
    self.willChange()
    self._type=object
  
  

