
from MetaQL.Base.FileNode import *
import dircache

# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class DirectoryNode (FileNode):
    
    # ----------------------------------------------------------------------------------------------
    def hasValidPath (self):
        """returns true, if the node exists and it's path is a directory"""
        path = self.getPathName()
        return os.path.exists(path) and os.path.isdir(path)

    # ----------------------------------------------------------------------------------------------
    def getChildren(self):
        """returns the children of this node"""
        path = self.getPathName()
        listdir = dircache.listdir(path)
        children = []
        for child in listdir:
            children.append(FileNode(self, child))
        return children

    # ----------------------------------------------------------------------------------------------
    def getChildDirectories(self):
        """returns child directories"""
        return filter(lambda x: x.__class__ == DirectoryNode, self.getChildren())

    # ----------------------------------------------------------------------------------------------
    def hasChildren(self):
        """returns true if the directoy isn't empty"""
        return len(self.getChildren())

    # ----------------------------------------------------------------------------------------------
    def hasSubdirectories(self):
        """returns true if the directoy has child directories"""
        return len(self.getChildDirectories())
    
    # ----------------------------------------------------------------------------------------------
    def getChildWithName(self, childName):
        """returns the child node with name childName"""
        children = self.getChildren()        
        for child in children:
            if child.getName() == childName:
                return child
        return None

    # ----------------------------------------------------------------------------------------------
    def isActionValidForNode (self, action, node):
        """returns True, if the node is accepted for action
            action should be from wxDragCopy, wxDragMove or wxDragNone"""
        from MetaQL.Base.Globals import debug
        debug(action)
        if action <= wxDragNone: return True
        
        debug(self.getPath())
        debug(self.getPath().access(os.W_OK))
        
        if node <> self and self.getPath().access(os.W_OK):   # different and writable
            if action == wxDragMove:
                return node.getPath().access(os.W_OK)
            else:
                return True
