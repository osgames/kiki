
from wxPython.wx import *
from MetaQL.Base.MQLNode import *
from MetaQL.Base.Globals import debug
 
# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class MQLNodeDropTarget(wxPyDropTarget):
    """
      DnD target for MetaQL nodes
    """

    # ----------------------------------------------------------------------------------------------
    def __init__(self, target=None):

        wxPyDropTarget.__init__(self)
        self.target = target
        self.dataFormat = wxCustomDataFormat ("MQL_NODE")
        self.data = wxCustomDataObject (self.dataFormat)
        self.SetDataObject (self.data)

    # ----------------------------------------------------------------------------------------------
    def OnDrop (self, x, y):
##        debug ("OnDrop: %d %d\n" % (x, y))
        debug (self.GetDataObject())
        return True

    # ----------------------------------------------------------------------------------------------
    def OnDragOver (self, x, y, d):
##        debug ("OnDragOver: %d, %d, %d\n" % (x, y, d))
        if hasattr (self.target, 'OnDragNode'):
            from MetaQL.Base.NodeManager import NodeManager
            return self.target.OnDragNode (x, y, NodeManager().draggedNode, d)
        return d
        
    # ----------------------------------------------------------------------------------------------
    # Called when OnDrop returns True
    def OnData(self, x, y, d):
##        debug ("OnData: %d, %d, %d\n" % (x, y, d))

        # copy the data from the drag source to our data object
        if self.GetData():
            # convert it back to a node and give it to the viewer
            nodeData = self.data.GetData()
            import cPickle
            node = cPickle.loads (nodeData)
            node._parent = None
            self._children = []
            if hasattr (self.target, 'OnDropNode'):
                self.target.OnDropNode (x, y, node)
        return d  # what is returned signals the source what to do
                  # with the original data (move, copy, etc.)  In this
                  # case we just return the suggested value given to us.