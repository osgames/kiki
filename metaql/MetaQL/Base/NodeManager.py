
import path

# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------

class NodeManager:

    __shared_state = {}
    
    # ----------------------------------------------------------------------------------------------
    def __init__(self):
        """the 'borg factory' for nodes?"""
        self.__dict__ = self.__shared_state
        if not hasattr(self, 'pathNodes'):
            self.pathNodes = {}
        if not hasattr(self, 'draggedNode'):
            self.draggedNode = None
        
    # ----------------------------------------------------------------------------------------------
    def GetNodeForFilePath (self, filePath):
        """returns a node for filePath"""
        path = path.path(filePath).expand()             # expand and normalize filePath
        
        if path in self.pathNodes:                      # check dict for existing path
            return self.pathNodes[path]                     # return it, if exists
        
        if path == path.parent:                         # if parent equals path
            parent = None                                   # root path -> parent = None
        else:                                               # otherwise retrieve parent node
            parent = FileNode (self.GetNodeForFilePath(path.parent))
        
        from MetaQL.Base.FileNode import FileNode
            
        fileNode = FileNode(parent, path.baseName())    # create the node
        dict[path] = fileNode                           # save node for path in dict
        return fileNode                                 # and return it

    # ----------------------------------------------------------------------------------------------
