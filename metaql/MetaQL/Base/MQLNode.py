# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
MQLNodeType_None, MQLNodeType_Directory, MQLNodeType_File = range(3)

# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class MQLNode:
    """
      Base class for all MetaQL nodes
    """

    # ----------------------------------------------------------------------------------------------
    def __init__(self):
        "Initializer"
        self._alias = None
        self._date = None
        self._name = None
        self._size = None
        self._typeId = None
        self._children=[]
        self._parent=None
        self._type=None

    # ----------------------------------------------------------------------------------------------
    def getAlias(self):
        "Return the Node / alias attribute value"
        return self._alias

    def setAlias(self, alias):
        "Change the Node / alias attribute value"
        self._alias = alias

    # ----------------------------------------------------------------------------------------------
    def getDate(self):
        "Return the Node / date attribute value"
        return self._date

    def setDate(self, date):
        "Change the Node / date attribute value"
        self._date = date

    # ----------------------------------------------------------------------------------------------
    def getName(self):
        "Return the Node / name attribute value"
        return self._name

    def setName(self, name):
        "Change the Node / name attribute value"
        self._name  = name

    # ----------------------------------------------------------------------------------------------
    def getSize(self):
        "Return the Node / size attribute value"
        return self._size

    def setSize(self, size):
        "Change the Node / size attribute value"
        self._size  = size

    # ----------------------------------------------------------------------------------------------
    def getTypeId(self):
        "Return the Node / typeId attribute value"
        return self._typeId

    def setTypeId(self, typeId):
        "Change the Node / typeId attribute value"
        self._typeId  = typeId

    # ----------------------------------------------------------------------------------------------
    def getChildren(self):
        "Returns the children relationship (toMany)"
        return self._children

    def addToChildren(self, object):
        "Add the children relationship (toMany)"
        if object not in self._children:
            _children=list(self._children)
            _children.append(object)
            self._children=tuple(_children)

    def removeFromChildren(self, object):
        "Remove the children relationship (toMany)"
        _children=list(self._children)
        _children.remove(object)
        self._children=tuple(_children)

    # ----------------------------------------------------------------------------------------------
    def getParent(self):
        "Return the parent relationship (toOne)"
        return self._parent

    def setParent(self, object):
        "Set the parent relationship (toOne)"
        self._parent=object

    # ----------------------------------------------------------------------------------------------
    def getType(self):
        "Return the type relationship (toOne)"
        return self._type

    def setType(self, object):
        "Set the type relationship (toOne)"
        self._type=object

    # ----------------------------------------------------------------------------------------------
    def __getstate__(self):
        """prevents the children and parent object from pickling"""
        odict = self.__dict__.copy() # copy the dict since we change it
        if '_children' in odict:
            del odict['_children']       # remove children
        if '_parent' in odict:
            del odict['_parent']         # remove parent
        return odict

    # ----------------------------------------------------------------------------------------------
    def getIconBitmap(self, size=(32,32)):
        from wxPython.wx import *
        return wxArtProvider_GetBitmap (wxART_WARNING, wxART_OTHER, size)
    
    