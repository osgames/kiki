
from MetaQL.Base.Globals import *

# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class SashChildSelector(wxListView):
    
    # ----------------------------------------------------------------------------------------------
    def __init__(self, parent, id=-1, style=0):
    
        wxListView.__init__ (self, parent, id=id, style=style)

        self.itemDataMap = {}
        self.SetWindowStyleFlag (wxLC_LIST)
        
        # initialize image list
        self.imageList  = wxImageList(16, 16)
        self.SetImageList (self.imageList, wxIMAGE_LIST_SMALL)
        
        index = 0
        
        for view in MQLSashViews:
            iconId = self.imageList.Add (wxArtProvider_GetBitmap(view['icon'], wxART_OTHER, (16,16)))
            self.InsertImageStringItem (index, ' ' + view['name'], iconId)
            index = index + 1

        EVT_LIST_ITEM_ACTIVATED (self, self.GetId(), self.itemActivated)

    # ----------------------------------------------------------------------------------------------
    def itemActivated (self, event):

        viewClass = MQLSashViews[event.GetIndex()]['class']
        exec 'from MetaQL.GUI.%s import %s' % (viewClass, viewClass)
        exec 'self.GetParent().child = ' + viewClass + '(self.GetParent())'
        self.GetParent().child.MoveXY (2,2)
        self.GetParent().OnSize (None)
        self.Destroy()
                
                