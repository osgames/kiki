
import cStringIO

from MetaQL.Base.Globals import *

[wxID_FILE_LIST_MENU_ITEM_0, 
 wxID_FILE_LIST_MENU_ITEM_1, 
 wxID_FILE_LIST_MENU_ITEM_2 
] = map(lambda _init_coll_fileMenu_Items: wxNewId(), range(3))

VIEW_MODE_ICONS, VIEW_MODE_LIST, VIEW_MODE_DETAILS = range(3)

from wxPython.lib.mixins.listctrl import wxColumnSorterMixin, wxListCtrlAutoWidthMixin

# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class FileList (wxListCtrl, wxColumnSorterMixin, wxListCtrlAutoWidthMixin):
    
    # ----------------------------------------------------------------------------------------------
    def __init__(self, parent, id=-1, name="FileList", pos=wxPoint(0, 0), size=wxSize(0, 0), \
                    style=0, validator=wxDefaultValidator):
        
        wxListCtrl.__init__(self, parent, id, pos, size, style, validator, name)
        
        # initialize icons
        imageSize = (16,16)
        self.imageList      = wxImageList(imageSize[0], imageSize[1])
        self.iconFolder     = self.imageList.Add(wxArtProvider_GetBitmap(wxART_FOLDER,      wxART_OTHER, imageSize))
        self.iconFolderOpen = self.imageList.Add(wxArtProvider_GetBitmap(wxART_FILE_OPEN,   wxART_OTHER, imageSize))
        self.iconFile       = self.imageList.Add(wxArtProvider_GetBitmap(wxART_NORMAL_FILE, wxART_OTHER, imageSize))

        self.SetImageList (self.imageList, wxIMAGE_LIST_SMALL)

        imageSizeLarge = (32,32)
        self.imageListLarge      = wxImageList(imageSizeLarge[0], imageSizeLarge[1])
        self.iconFolderLarge     = self.imageListLarge.Add(wxArtProvider_GetBitmap(wxART_FOLDER,      wxART_OTHER, imageSizeLarge))
        self.iconFolderOpenLarge = self.imageListLarge.Add(wxArtProvider_GetBitmap(wxART_FILE_OPEN,   wxART_OTHER, imageSizeLarge))
        self.iconFileLarge       = self.imageListLarge.Add(wxArtProvider_GetBitmap(wxART_NORMAL_FILE, wxART_OTHER, imageSizeLarge))
        self.SetImageList(self.imageListLarge, wxIMAGE_LIST_NORMAL)
                
        self.directory = None

        self.setDisplayModeDetails()

        from MetaQL.Base.MQLNodeDropTarget import MQLNodeDropTarget
        self.SetDropTarget (MQLNodeDropTarget(self))
  
        EVT_RIGHT_DOWN          (self, self.popupMenu)
        EVT_LIST_ITEM_ACTIVATED (self, self.GetId(), self.OnItemActivated)
        EVT_LIST_BEGIN_DRAG     (self, self.GetId(), self.OnBeginDrag) 

        wxListCtrlAutoWidthMixin.__init__(self)
        wxColumnSorterMixin.__init__(self, 5)
        
        NotificationCenter.addObserver(self, FileList.nodeActivated, "MQL_NODE_ACTIVATED")
        NotificationCenter.addObserver(self, FileList.nodeSelected,  "MQL_NODE_SELECTED")

    # ----------------------------------------------------------------------------------------------
    def GetNodeAtIndex (self, index):
        itemName = self.GetItem(index, self.viewMode == VIEW_MODE_DETAILS).GetText()
        return self.directory.getChildWithName (itemName)

    # ----------------------------------------------------------------------------------------------
    def OnDropNode (self, x, y, node):       
        debug ("%d %d %s" % (x, y, str(node)))
        return False

    # ----------------------------------------------------------------------------------------------
    def OnBeginDrag (self, event):
        debug (event.GetIndex())
        node = self.GetNodeAtIndex (event.GetIndex())
        from MetaQL.Base.MQLNodeDropSource import *
        result = MQLNodeDropSource (self, node).DoDragDrop(True)
        debug ("dnd result" + str(result))

    # ----------------------------------------------------------------------------------------------
    def GetListCtrl (self):
        """method needed for wxColumnSorterMixin"""
        return self

    # ----------------------------------------------------------------------------------------------
    def nodeActivated (self, notification):
        debug()
        node = notification.object()
        if node.__class__ == DirectoryNode:
            self.displayDirectory(node)

    # ----------------------------------------------------------------------------------------------
    def nodeSelected (self, notification):
        debug()
        node = notification.object()
        if node.getParent() == self.directory:
            item = self.FindItem (0, node.getName())
            self.EnsureVisible (item)
            
    # ----------------------------------------------------------------------------------------------
    def displayDirectory (self, dirObject):

        self.DeleteAllItems()
        
        if not dirObject: return

        self.directory = dirObject
        
        index = 0;
                
        if self.viewMode == VIEW_MODE_ICONS:
            idFolderIcon = self.iconFolderLarge
            idFileIcon = self.iconFileLarge
        else:
            idFolderIcon = self.iconFolder
            idFileIcon = self.iconFile
        
        self.itemDataMap = {}
        
        for child in dirObject.getChildren():
            
            iconId = idFolderIcon
            
            if child.__class__ == FileNode:
                iconId = idFileIcon
                
            if self.viewMode == VIEW_MODE_DETAILS:
                self.InsertImageItem (index, iconId)
                self.SetStringItem (index, 1, child.getName())
                self.SetStringItem (index, 2, str(child.getSize()))
                self.SetStringItem (index, 3, child.getAlias())
                self.SetStringItem (index, 4, str(child.getDate()))
                
                # for column sorting
                self.itemDataMap[index] = [child.getTypeId(), child.getName(), child.getSize(), \
                                            child.getAlias(), child.getDate()]
                self.SetItemData (index, index)
            else:
                self.InsertImageStringItem (index, child.getName(), iconId)
        
            index = index + 1
                
    # ----------------------------------------------------------------------------------------------
    def popupMenu (self, event):
        
        menu = wxMenu (title='')
        menu.Append (helpString='', id=wxID_FILE_LIST_MENU_ITEM_0, item='Icons', kind=wxITEM_NORMAL)
        menu.Append (helpString='', id=wxID_FILE_LIST_MENU_ITEM_1, item='List', kind=wxITEM_NORMAL)
        menu.Append (helpString='', id=wxID_FILE_LIST_MENU_ITEM_2, item='Details', kind=wxITEM_NORMAL)
              
        EVT_MENU (self, wxID_FILE_LIST_MENU_ITEM_0, self.setDisplayModeIcons)
        EVT_MENU (self, wxID_FILE_LIST_MENU_ITEM_1, self.setDisplayModeList)
        EVT_MENU (self, wxID_FILE_LIST_MENU_ITEM_2, self.setDisplayModeDetails)
        
        self.PopupMenu(menu, event.GetPosition())
        
    # ----------------------------------------------------------------------------------------------
    def setDisplayModeIcons (self, event=None):
                
        self.SetWindowStyleFlag(wxLC_ICON)
        self.viewMode = VIEW_MODE_ICONS
        self.refresh()
    
    # ----------------------------------------------------------------------------------------------
    def setDisplayModeList (self, event=None):        
        
        self.SetWindowStyleFlag(wxLC_LIST)
        self.viewMode = VIEW_MODE_LIST
        self.refresh()
        
    # ----------------------------------------------------------------------------------------------
    def setDisplayModeDetails (self, event=None):
        
        self.ClearAll() 
        self.SetWindowStyleFlag(wxLC_REPORT) 
        
        self.InsertColumn(0, '')
        self.InsertColumn(1, 'name')
        self.InsertColumn(2, 'size')
        self.InsertColumn(3, 'alias')
        self.InsertColumn(4, 'date')

        self.SetColumnWidth(0, 22)

        self.viewMode = VIEW_MODE_DETAILS
        self.refresh()
                        
    # ----------------------------------------------------------------------------------------------
    def OnItemActivated (self, event):
        NotificationCenter.postNotification("MQL_NODE_ACTIVATED", self.GetNodeAtIndex(event.GetIndex()))
                
    # ----------------------------------------------------------------------------------------------
    def refresh (self):        
        self.displayDirectory (self.directory)
