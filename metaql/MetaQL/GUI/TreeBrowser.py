
from MetaQL.Base.Globals import *

# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class TreeBrowser(wxTreeCtrl):

    # ----------------------------------------------------------------------------------------------
    def __init__(self, parent, id=-1, name="TreeBrowser", pos=wxPoint(0, 0), size=wxSize(0, 0), style=wxTR_HAS_BUTTONS, validator=wxDefaultValidator):
        
        wxTreeCtrl.__init__(self, parent, id, pos, size, style, validator, name)
 
        # initialize icons
        imageSize = (16,16)
        self.imageList      = wxImageList(imageSize[0], imageSize[1])
        self.iconFolder     = self.imageList.Add(wxArtProvider_GetBitmap(wxART_FOLDER,      wxART_OTHER, imageSize))
        self.iconFolderOpen = self.imageList.Add(wxArtProvider_GetBitmap(wxART_FILE_OPEN,   wxART_OTHER, imageSize))
        self.iconFile       = self.imageList.Add(wxArtProvider_GetBitmap(wxART_NORMAL_FILE, wxART_OTHER, imageSize))
        
        self.SetImageList(self.imageList)

        from MetaQL.Base.MQLNodeDropTarget import MQLNodeDropTarget
        self.SetDropTarget (MQLNodeDropTarget(self))
        
        EVT_TREE_ITEM_EXPANDING (self, self.GetId(), self.OnItemExpanding)
        EVT_TREE_SEL_CHANGED    (self, self.GetId(), self.OnSelChanged)
        EVT_TREE_BEGIN_DRAG     (self, self.GetId(), self.OnBeginDrag)
        
        self.root = None  
        self.setRootObject (FileNode (None, '/'))

    # ----------------------------------------------------------------------------------------------
    def OnDropNode (self, x, y, node):
        
        debug ("%d %d %s" % (x, y, str(node)))
        return False
        
    # ----------------------------------------------------------------------------------------------
    def OnItemExpanding (self, event):
        
        item = event.GetItem()

        if self.GetChildrenCount(item, False):
            # if item has children -> it has been expanded before -> nothing to do
            return

        childObject = self.GetPyData(item)
        childObjects = childObject.getChildDirectories()
        
        for childObject in childObjects:
            child = self.AppendItem(item, childObject.getName(), self.iconFolder, \
                                    self.iconFolderOpen, data=wxTreeItemData(childObject))
            self.SetItemHasChildren(child, childObject.hasSubdirectories())

    # ----------------------------------------------------------------------------------------------
    def OnSelChanged (self, event):
        ##debug()
        NotificationCenter.postNotification("MQL_NODE_ACTIVATED", self.GetPyData(event.GetItem()))
        
    # ----------------------------------------------------------------------------------------------
    def OnBeginDrag (self, event):
        node = self.GetPyData(event.GetItem())
        from MetaQL.Base.MQLNodeDropSource import *
        result = MQLNodeDropSource (self, node).DoDragDrop(True)
        debug ("dnd result" + str(result))
                                        
    # ----------------------------------------------------------------------------------------------
    def setRootObject (self, rootObject):
                
        if self.root:
            self.Delete (self.root)
                            
        if not rootObject: 
            return

        self.root = self.AddRoot (rootObject.getName(), self.iconFolder, self.iconFolderOpen, \
                                    data=wxTreeItemData (rootObject))
        self.SetItemHasChildren (self.root, True)
                
        # expand the root item
        self.Expand (self.root)

