
from MetaQL.Base.Navigation import *
from MetaQL.Base.Renderer import *
from MetaQL.Base.Globals import *

from wxPython.glcanvas import *
from OpenGL.GL import *
from OpenGL.GLUT import *

from OpenGLContext.wxinteractivecontext import wxInteractiveContext
from OpenGLContext.scenegraph.basenodes import * 
from OpenGLContext.renderpass import * 
from OpenGLContext.debug.logs import * 
from OpenGLContext.events.wxevents import *

# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class PickHandler:
    
    # ----------------------------------------------------------------------------------------------
    def __init__(self):
        
        self.pickables = []
    
    # ----------------------------------------------------------------------------------------------
    def addPickable (self, pickable):
    
        self.pickables.append (pickable)
        return len(self.pickables)-1
    
    # ----------------------------------------------------------------------------------------------
    def getPickable (self, pickable_id):
        
        return self.pickables[pickable_id]

    
# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class OpenGLView (wxInteractiveContext, PickHandler):

    # ----------------------------------------------------------------------------------------------
    def __init__(self, parent, id=-1, name="OpenGLView", style=0):
        
        wxInteractiveContext.__init__(self, parent=parent, id=id, name=name, style=style)
        PickHandler.__init__(self)
        
        from MetaQL.Base.MQLNodeDropTarget import MQLNodeDropTarget
        self.SetDropTarget (MQLNodeDropTarget(self))

        self.node = None
        self.navigationMode = False
        self.pickedNode = None
        
        NotificationCenter.addObserver(self, OpenGLView.nodeActivated, "MQL_NODE_ACTIVATED")

    # ----------------------------------------------------------------------------------------------
    def OnDropNode (self, x, y, node):
        
        debug ("%d %d %s" % (x, y, str(node)))
        
    # ----------------------------------------------------------------------------------------------
    def OnInit (self):
        
        debug ()
        self.setRenderer(FileTreeRenderer(self))

        glShadeModel(GL_SMOOTH)
        glCullFace	(GL_BACK)
    
        glEnable (GL_CULL_FACE)
        glEnable (GL_DEPTH_TEST)
        glEnable (GL_NORMALIZE)
        glEnable (GL_COLOR_MATERIAL)
    
        glDepthRange (0.0, 0.5)
        
        white   = (1.0, 1.0, 1.0, 1.0)
        bright  = (0.75, 0.75, 0.75, 1.0)
        gray    = (0.5, 0.5, 0.5, 1.0)
        dark    = (0.25, 0.25, 0.25, 1.0)
        
        glLightModeli  (GL_LIGHT_MODEL_LOCAL_VIEWER, 1)

        glColorMaterial (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE)

        glMaterialfv (GL_FRONT_AND_BACK, GL_SPECULAR,  bright)
        glMaterialf  (GL_FRONT_AND_BACK, GL_SHININESS, 100.0)
            
        light = GL_LIGHT0
        glLightfv (light, GL_SPECULAR,  white)
        glLightfv (light, GL_AMBIENT,   dark)
        glLightfv (light, GL_DIFFUSE,   gray)
        glLightf  (light, GL_CONSTANT_ATTENUATION,  1.0)
        glLightf  (light, GL_LINEAR_ATTENUATION,    0.0)
        glLightf  (light, GL_QUADRATIC_ATTENUATION, 0.0)
        glLightf  (light, GL_SPOT_CUTOFF, 90.0)
        glEnable  (light)
                        
        glEnable (GL_LIGHTING)

    # ----------------------------------------------------------------------------------------------
    def setNavigationMode (self, on):   
        self.navigationMode = on     
        if self.navigationMode:
            self.CaptureMouse()
        else:
            self.ReleaseMouse()
                
    # ----------------------------------------------------------------------------------------------
    def startRotateMode (self, event):
        RotateManager (self, self.platform, (0.0, 0.0, 0.0), event)

    # ----------------------------------------------------------------------------------------------
    def startZoomMode (self, event):
        ZoomManager (self, self.platform, (0.0, 0.0, 0.0), event)

    # ----------------------------------------------------------------------------------------------
    def startMoveMode (self, event):
        MoveManager (self, self.platform, (0.0, 0.0, 0.0), event)

    # ----------------------------------------------------------------------------------------------
    def setupDefaultEventCallbacks (self):
        self.addEventHandler( 'mousebutton', button=0, state=1, modifiers=(0,0,0), function=self.startRotateMode)
        self.addEventHandler( 'mousebutton', button=2, state=1, modifiers=(0,0,0), function=self.startZoomMode)
        self.addEventHandler( 'mousebutton', button=1, state=1, modifiers=(0,0,0), function=self.startMoveMode)

    # ----------------------------------------------------------------------------------------------
    def nodeActivated (self, notification):
        debug(str(notification))
        node = notification.object()
        
        if node.__class__ == DirectoryNode:
            self.displayDirectory(node)

    # ----------------------------------------------------------------------------------------------
    def nodeDoubleClicked (self, node):
        if node == self.node:
            if node.getParent():
                node = node.getParent()
        NotificationCenter.postNotification("MQL_NODE_ACTIVATED", node)

    # ----------------------------------------------------------------------------------------------
    def nodeSingleClicked (self, node):
        NotificationCenter.postNotification("MQL_NODE_SELECTED", node)
                        						               
    # ----------------------------------------------------------------------------------------------
    def displayDirectory (self, node):
        self.node = node
        self.renderer.reset()
        self.Refresh (True)

    # ----------------------------------------------------------------------------------------------
    def setRenderer (self, renderer):
        self.renderer = renderer
        self.Refresh (True)
            
    # ----------------------------------------------------------------------------------------------
    def Background (self, mode=None):
        if mode.passCount == 0: # first pass...
            glClearColor (0.0,0.0,0.0,1.0)
            glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
			        
    # ----------------------------------------------------------------------------------------------
    def Render (self, mode=None):             
        if self.node and self.renderer:
            self.renderer.renderNode (self.node)
        
    # ----------------------------------------------------------------------------------------------
    def ProcessEvent (self, event):  

        if self.navigationMode:
            # don't interfere in navgigation mode
            wxInteractiveContext.ProcessEvent (self, event)
            return
        
        if hasattr(event, 'getNameStack'):
            if len(event.getNameStack()):
                                
                nameStack = event.getNameStack()                               
                nameStack.sort()
                                
                picked = self.getPickable(nameStack[0][-1][-1])
                
                statusText = picked.getName() + " (" + picked.getPathName() + ")"
                wxGetTopLevelParent(self).SetStatusText(statusText)

                tip = wxTipWindow (self, picked.getPathName())
                posx, posy = wxGetMousePosition()
                tip.SetBoundingRect (wxRect(posx, posy, 2, 2))
                                
                if event.type == 'mousebutton' and event.state:
                    pickTime = time.clock()
                    if self.pickedNode and \
                        self.pickedNode[0] == picked and \
                        pickTime - self.pickedNode[1] < 0.5:
                        # node 'double'-clicked
                        self.nodeDoubleClicked (picked)
                    else:
                        # node 'single'-clicked
                        self.nodeSingleClicked (picked)
                    self.pickedNode = (picked, pickTime)
                    return
            else:
                if self.pickedNode and event.type == 'mousemove' and event.buttons == (0,):
                                        
                    from MetaQL.Base.MQLNodeDropSource import *
                    result = MQLNodeDropSource (self, self.pickedNode[0]).DoDragDrop(True)
                    
                    debug ("%s (c %s) (m %s)" % (str(result), str(wxDragCopy), str(wxDragMove)))
                    if result == wxDragCopy:
                        debug ("node copied")
                    elif result == wxDragMove:
                        debug ("node moved")
                    else:
                        debug ("drag failed")
                else:
                    self.pickedNode = None
                
        wxInteractiveContext.ProcessEvent (self, event)
            
    # ----------------------------------------------------------------------------------------------
    def OnIdle (self):
		#self.triggerRedraw(1)
		return 1    


