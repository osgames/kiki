
from wxPython.wx import *
from MetaQL.Base.Globals import *

# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class DragShape:

    # ----------------------------------------------------------------------------------------------
    def __init__(self, node):
        self.node = node
        self.bmp = node.getIconBitmap()
        self.pos = wxPoint(0,0)
        self.shown = True
        self.fullscreen = False
    # ----------------------------------------------------------------------------------------------
    def HitTest(self, pt):
        rect = self.GetRect()
        return rect.InsideXY(pt.x, pt.y)
    # ----------------------------------------------------------------------------------------------
    def GetRect(self):
        return wxRect(self.pos.x, self.pos.y, self.bmp.GetWidth(), self.bmp.GetHeight())
    
    # ----------------------------------------------------------------------------------------------
    def Draw(self, dc, op = wxCOPY):
        memDC = wxMemoryDC()
        memDC.SelectObject(self.bmp)
        dc.Blit (self.pos.x, self.pos.y, self.bmp.GetWidth(), self.bmp.GetHeight(), memDC, 0, 0, op, True)
        tw, th = dc.GetTextExtent(self.node.getName())
        dc.DrawText(self.node.getName(), self.pos.x + self.bmp.GetWidth() / 2 - tw/2, \
                            self.pos.y + self.bmp.GetHeight())
        return True
# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class Shelf (wxScrolledWindow):
    
    # ----------------------------------------------------------------------------------------------
    def __init__(self, parent, id=-1, name='Shelf', style=0):
        
        wxScrolledWindow.__init__(self,  parent=parent, id=id, name=name, style=style)
        
        from MetaQL.Base.MQLNodeDropTarget import MQLNodeDropTarget
        self.SetDropTarget (MQLNodeDropTarget(self))
        
        self.unclean = False
        self.shapes = []
        self.dragStartPos = None
        
        EVT_ERASE_BACKGROUND(self, self.OnEraseBackground)
        EVT_PAINT       (self, self.OnPaint)
        EVT_LEFT_DOWN   (self, self.OnLeftDown)
        EVT_LEFT_UP     (self, self.OnLeftUp)
        EVT_MOTION      (self, self.OnMotion)

    # ----------------------------------------------------------------------------------------------
    def OnEraseBackground (self, evt):
        
        dc = evt.GetDC()
        if not dc:
            dc = wxClientDC(self)
            rect = self.GetUpdateRegion().GetBox()
            dc.SetClippingRegion(rect.x, rect.y, rect.width, rect.height)
        dc.Clear()
        
    # ----------------------------------------------------------------------------------------------
    def OnDragNode (self, x, y, node, action=None):
##        debug (action)
##        debug ("%d %d %s" % (x, y, str(node)))
        # possibly highlighting another shape if dragging
        shape = self.FindShapeAtPos(wxPoint(x, y))
        if shape:
            debug (shape)
            debug (node)
            debug (shape.node)
            if shape.node == node:
                action = wxDragNone
            else:
                for action in range(wxDragMove,wxDragError,-1):
                    if shape.node.isActionValidForNode (action, node):
                        debug("------------- action")
                        break
                    
##            unhiliteOld = False
##            hiliteNew = False
##            # figure out what to hilite and what to unhilite
##            if self.hiliteShape:
##                if onShape is None or self.hiliteShape is not onShape:
##                    unhiliteOld = True
##            if onShape and onShape is not self.hiliteShape and onShape.shown:
##                hiliteNew = True
##            # if needed, hide the drag image so we can update the window
##            if unhiliteOld or hiliteNew:
##                self.dragImage.Hide()
##            if unhiliteOld:
##                dc = wxClientDC(self)
##                self.hiliteShape.Draw(dc)
##                self.hiliteShape = None
##            if hiliteNew:
##                dc = wxClientDC(self)
##                self.hiliteShape = onShape
##                self.hiliteShape.Draw(dc, wxINVERT)
##            # now move it and show it again if needed
##            self.dragImage.Move(evt.GetPosition())
##            if unhiliteOld or hiliteNew:
##                self.dragImage.Show()
        return action
        
    # ----------------------------------------------------------------------------------------------
    def OnDropNode (self, x, y, node):
        
        debug ("%d %d %s" % (x, y, str(node)))
        shape = self.FindShapeForNode(node)
        if shape:
            shape.pos = wxPoint (x, y)
            self.unclean = True
            return True
        
        shape = DragShape (node)
        shape.pos = wxPoint (x, y)
        self.shapes.append (shape)
        self.unclean = True
        return False
    
    # ----------------------------------------------------------------------------------------------
    def DrawShapes (self, dc):
        
        for shape in self.shapes:
            if shape.shown:
                shape.Draw(dc)
                
    # ----------------------------------------------------------------------------------------------
    def Redraw (self):
        dc = wxClientDC(self)
        self.PrepareDC(dc)
        dc.Clear()
        self.DrawShapes(dc)
        self.unclean = False
        
    # ----------------------------------------------------------------------------------------------
    def FindShapeAtPos (self, pos):
        for shape in self.shapes:
            if shape.HitTest(pos):
                return shape
        return None

    # ----------------------------------------------------------------------------------------------
    def FindShapeForNode (self, node):
        for shape in self.shapes:
            if shape.node.getPathName() == node.getPathName():
                return shape
        return None

    # ----------------------------------------------------------------------------------------------
    def EraseShape (self, shape, dc):

        r = shape.GetRect()
        dc.SetClippingRegion(r.x, r.y, r.width, r.height)
        self.DrawShapes(dc)
        dc.DestroyClippingRegion()
        
    # ----------------------------------------------------------------------------------------------
    def OnPaint (self, evt):
        
        dc = wxPaintDC(self)
        self.PrepareDC(dc)
        dc.Clear()
        self.DrawShapes(dc)
        
    # ----------------------------------------------------------------------------------------------
    def OnLeftDown (self, event):
        
        shape = self.FindShapeAtPos (event.GetPosition())
        if shape:
            self.dragNode = shape.node
            self.dragStartPos = event.GetPosition()
    # ----------------------------------------------------------------------------------------------
    def OnLeftUp (self, evt):

        dc = wxClientDC(self)
        dc.Clear()
##        if self.hiliteShape:
##            self.hiliteShape.Draw(dc)
##            self.hiliteShape = None
        self.DrawShapes(dc)
        
    # ----------------------------------------------------------------------------------------------
    def OnMotion (self, evt):

        if not evt.Dragging() or not evt.LeftIsDown():
            if self.unclean:
                self.Redraw()
            return
        if self.dragStartPos:
            # only start the drag after having moved a couple pixels
            tolerance = 2
            pt = evt.GetPosition()
            dx = abs(pt.x - self.dragStartPos.x)
            dy = abs(pt.y - self.dragStartPos.y)
            if dx <= tolerance and dy <= tolerance:
                return
            
            from MetaQL.Base.MQLNodeDropSource import *
            result = MQLNodeDropSource (self, self.dragNode).DoDragDrop(True)

            debug ("dnd result" + str(result))
            self.dragStartPos = None

