
from MetaQL.Base.Globals import *

# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
class Console(wxTextCtrl):
    
    # ----------------------------------------------------------------------------------------------
    def __init__(self, parent, id=-1, style=wxTE_MULTILINE|wxHSCROLL):
    
        wxTextCtrl.__init__ (self, parent, id=id, style=style)
        # set the text controll to use as many lines as possible
        self.SetMaxLength(0)
        # redirect log messages to console
        wxLog_SetActiveTarget (wxLogTextCtrl(self))
        # redirect std.err and std.out to console
        sys.stderr = self
        sys.stdout = self

        from MetaQL.Base.MQLNodeDropTarget import MQLNodeDropTarget
        self.SetDropTarget (MQLNodeDropTarget())

        EVT_CLOSE (self, self.Destroy)

    # ----------------------------------------------------------------------------------------------
    def __del__ (self):
        
        debug ("__del__")
                
    # ----------------------------------------------------------------------------------------------
    def Destroy (self, *_args, **_kwargs ):
   
        # restore std.err and std.out
        sys.stderr = sys.__stderr__
        sys.stdout = sys.__stdout__
        # restore wxLog
        wxLog_SetActiveTarget (wxLogGui())
        debug("Console Destroyed")
        wxTextCtrl.Destroy (self)